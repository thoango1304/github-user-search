import React from "react";
import IconLocation from "../assets/images/icon-location.svg";
import IconWebsite from "../assets/images/icon-website.svg";
import IconTwitter from "../assets/images/icon-twitter.svg";
import IconCompany from "../assets/images/icon-company.svg";

class ContentBottom extends React.Component {
    constructor(props) {
        super(props);
        this.createLinkElement = this.createLinkElement.bind(this);
        this.createNoLinkElement = this.createNoLinkElement.bind(this);
    }

    createLinkElement(propName, linkText) {
        let available;
        let element;

        if (propName == null || propName == "") {
            available = "Not Available";
            return element = (
                <span className="noAvailableLink">{available}</span>
            );
        } else {
            available = propName;
            return element = (
                <a href={linkText + available} target="_blank">{available}</a>
            );
        }
    }

    createNoLinkElement(propName) {
        let available;
        let element;

        if (propName == null || propName == "") {
            available = "Not Available";
            return element = (
                <span className="noAvailableLink">{available}</span>
            );
        } else {
            available = propName;
            return element = (
                <span>{available}</span>
            );
        }
    }

    render() {
        const user = this.props.user;
        const twitterHomePage = "#https://twitter.com/";
        const home = "#";
        let location = this.createNoLinkElement(user.location);
        let company = this.createNoLinkElement(user.company);
        let blog = this.createLinkElement(user.blog, home);
        let twitter = this.createLinkElement(user.twitter_username, twitterHomePage);

        return (
            <div className="content-links" id="contentBottom">
                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="link">
                            <IconLocation />
                            {location}
                        </div>
                        <div className="link link-bottom">
                            <IconWebsite />
                            {blog}
                        </div>
                    </div>
                    <div className="col-12 col-md-6">
                        <div className="link">
                            <IconTwitter />
                            {twitter}
                        </div>
                        <div className="link link-bottom">
                            <IconCompany />
                            {company}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ContentBottom;