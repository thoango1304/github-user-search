import React from "react";
import axios from "axios";
import Header from "./Header";
import SearchForm from "./SearchForm";
import Content from "./Content";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: "octocat",
            data: {},
            noData: false
        };
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
    }

    componentDidMount() {
        this.fetchData(this.state.user);
    }

    handleUsernameChange(username) {
        this.setState({ user: username }, function() {
            this.fetchData(this.state.user);
        });
    }

    fetchData(username) {
        let url = `https://api.github.com/users/${username}`;
        axios.get(url)
            .then(res => {
                this.setState({ data: res.data, noData: false });
            })
            .catch(err => {
                console.log(err);
                this.setState({ noData: true });
            })
    }

    render() {
        return (
            <div className="devfinder d-flex align-items-center">
                <div className="container p-0">
                    <Header />
                    <SearchForm noData={this.state.noData} username={this.state.user} onUsernameChange={this.handleUsernameChange} />
                    <Content user={this.state.data} />
                </div>
            </div>
        );
    }
}

export default App;