import React from "react";
import IconSearch from "../assets/images/icon-search.svg";

class SearchForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            error: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
    }

    handleInputChange(event) {
        this.setState({username: event.target.value});
    }

    handleUsernameChange() {
        if (this.state.username != "" && this.state.username != this.props.username) {
            this.props.onUsernameChange(this.state.username);
            this.setState({ error: false });
        } else if (this.state.username == "" || this.props.noData) {
            this.setState({error: true});
        }
    }

    render() {
        let noResults = "";
        if (this.state.error || this.props.noData) {
            noResults = (
                <div className="d-flex align-items-center">
                    <span className="input-error">No results</span>
                </div>
            );
        }
        return (
            <div className="searchForm" id="searchForm">
                <div className="input-group">
                    <span className="input-group-text">
                        <IconSearch />
                    </span>
                    <input type="text" className="form-control" placeholder="Search GitHub username..." value={this.state.username} onChange={this.handleInputChange} />
                    {noResults}
                    <button onClick={this.handleUsernameChange} className="btn btn btn-primary" id="btn-search" type="button">Search</button>
                </div>
            </div>
        );
    }
}

export default SearchForm;