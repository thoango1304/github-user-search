import React from "react";

class ContentMiddle extends React.Component {
    render() {
        const user = this.props.user;
        
        return (
            <div className="content-numbers" id="contentMiddle">
                <div className="d-flex justify-content-between">
                    <div>
                        <h4>Repos</h4>
                        <h2>{user.public_repos}</h2>
                    </div>
                    <div>
                        <h4>Followers</h4>
                        <h2>{user.followers}</h2>
                    </div>
                    <div>
                        <h4>Following</h4>
                        <h2>{user.following}</h2>
                    </div>
                </div>
            </div>
        );
    }
}

export default ContentMiddle;