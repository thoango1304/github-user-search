import React from "react";
import IconMoon from "../assets/images/icon-moon.svg";
import IconSun from "../assets/images/icon-sun.svg";

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {darkMode: false};
        this.toggleThemeMode = this.toggleThemeMode.bind(this);
    }

    toggleThemeMode() {
        const body = document.body;

        if (body.classList.contains("dark-mode")) {
            body.classList.remove("dark-mode");
            this.setState({darkMode: false});
        } else {
            body.classList.add("dark-mode");
            this.setState({darkMode: true});
        }
    }

    changeText() {
        if (this.state.darkMode) {
            return (
                <h4>
                    LIGHT
                    <IconSun />
                </h4>
            );
        } else {
            return (
                <h4>
                    DARK
                    <IconMoon />
                </h4>
            );
        }
    }

    render() {
        return (
            <div className="header">
                <div className="row">
                    <div className="col-6">
                        <h1>devfinder</h1>
                    </div>
                    <div className="col-6 d-flex align-items-center justify-content-end" id="toggle">
                        <input onClick={this.toggleThemeMode} type="checkbox" id="toggle-button" className="d-none" />
                        <label htmlFor="toggle-button">
                            {this.changeText()}
                        </label>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;