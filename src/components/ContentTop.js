import React from "react";

class ContentTop extends React.Component {
    formatDate(date) {
        const joinedDate = new Date(date);
        const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        return `${joinedDate.getDate()} ${months[joinedDate.getMonth()]} ${joinedDate.getFullYear()}`;
    }

    render() {
        const user = this.props.user;
        let bio = user.bio == null ? "This profile has no bio" : user.bio;
        let name = user.name == null ? user.login : user.name;

        return (
            <div className="row" id="contentTop">
                <div className="col-3">
                    <div className="content-avatar">
                        <img src={user.avatar_url} />
                    </div>
                </div>
                <div className="col-9">
                    <div className="content-info d-block d-lg-flex justify-content-between">
                        <div className="content-left">
                            <h1>{name}</h1>
                            <h3>@{user.login}</h3>
                        </div>
                        <div className="content-right">
                            <p>Joined {this.formatDate(user.created_at)}</p>
                        </div>
                    </div>
                    <div className="content-bio d-none d-lg-block">
                        <p>{bio}</p>
                    </div>
                </div>
                <div className="col-12">
                    <div className="content-bio d-block d-lg-none">
                        <p>{bio}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default ContentTop;