import React from "react";
import ContentTop from "./ContentTop";
import ContentMiddle from "./ContentMiddle";
import ContentBottom from "./ContentBottom";

class Content extends React.Component {
    render() {
        const user = this.props.user;
        
        return (
            <div className="content" id="content">
                <div className="row">
                    <div className="col-12">
                        <ContentTop user={user} />
                    </div>
                    <div className="col-12 col-lg-9 offset-lg-3">
                        <ContentMiddle user={user} />
                        <ContentBottom user={user} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Content;